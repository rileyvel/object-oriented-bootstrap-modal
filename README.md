# object-oriented-bootstrap-modal

Create completely standalone, shadow-DOM encapsulated bootstrap popups in JavaScript! This is especially well suited for
Userscript / Browser extension developers.

Please note that this loads Bootstrap CSS from JsDelivr when a popup is opened.

## Installation

### NPM

First, install it from npm: `npm i object-oriented-bootstrap-modal`

Import the main class: `import {ObjectOrientedBootstrapModal} from 'object-oriented-bootstrap-modal`

### CDN

A bundle.js file is provided for your convenience.

For example, to import `https://cdn.jsdelivr.net/npm/object-oriented-bootstrap-modal/bundle.js` as an ES module, either
by:

```js
import {ObjectOrientedBootstrapModal} from 'https://cdn.jsdelivr.net/npm/object-oriented-bootstrap-modal/bundle.js';
```

in a module type JS, or dynamically import like:

```js
import('https://cdn.jsdelivr.net/npm/object-oriented-bootstrap-modal/bundle.js').then(({ObjectOrientedBootstrapModal}) => {
    // ..do what you want with ObjectOrientedBootstrapModal
});
```

in any JS.

## Constructor Arguments

You can pass up to 3 arguments to the constructor:

* header: string. This is the title of the modal.
* content: string or HTMLElement. Optional. If provided, this will fill the content of the modal. You can always access this as a class field later.
* onConfirm: Function. Optional. This will be called when the user presses the confirm button, or ctrl+enter. You can access this later as a class field.

## Use

You can either instantiate the ObjectOrientedBootstrapModal class as is, or subclass it to suit your own needs.

Check out test.html for an example:

```js
import {ObjectOrientedBootstrapModal} from "./bundle.js";

const modal = new ObjectOrientedBootstrapModal({
    content: '<p>Woohoo, you did it!</p>',
    header: 'New Modal'
});

modal.onConfirm = () => {
    alert('Meow');
};

document.getElementById('button').addEventListener('click', () => {
    modal.open();
});
```

Or to try something a bit more elaborate:

```tsx
import {ObjectOrientedBootstrapModal} from "...";

export class MyModal extends ObjectOrientedBootstrapModal {
    constructor() {
        super({
            header: 'My super duper title'
        });
    }
    
    open() {
        super.open();
        ReactDOM.render(<FooComponent />, this.content);
    }
    
    close() {
        ReactDOM.unmountComponentAtNode(this.content);
        super.close();
    }
}

const FooComponent: React.FC = () => {
    return <h1>Hello world...</h1>
}
```
