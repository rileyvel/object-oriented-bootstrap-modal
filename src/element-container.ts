export class ElementContainer {

    constructor(private shadowHost: HTMLElement,
                private shadowRoot: ShadowRoot) {
    }

    private isOpen = false;

    public insertSelf() {
        if (!this.isOpen) {
            document.body.appendChild(this.shadowHost);
            this.getModalElement().classList.add('animate');
            this.isOpen = true;
        }
    }

    public removeSelf() {
        if (this.isOpen) {
            document.body.removeChild(this.shadowHost);
            this.isOpen = false;
        }
    }

    public getModalElement(): HTMLElement {
        return this.shadowRoot.querySelector('.modal-dialog')!;
    }

    public getContentElement(): HTMLElement {
        return this.shadowRoot.querySelector('.modal-body')!;
    }

    public addCloseButtonListener(onClose: () => any) {
        // Remember there are 2 close buttons
        this.shadowRoot.querySelectorAll('*[closeButton]')!
            .forEach(item => item.addEventListener('click', onClose));
    }

    public addConfirmButtonListener(onConfirm: () => any) {
        this.shadowRoot.querySelector('.btn.btn-primary[confirmButton]')!
            .addEventListener('click', onConfirm);
    }

}
