/**
 * This class specialises in listening to ctrl+enter events
 */
export class CtrlEnterListener {

    constructor(private callback: () => any) {
    }

    engage() {
        document.addEventListener('keypress', this.rawListener);
    }

    disengage() {
        document.removeEventListener('keypress', this.rawListener);
    }

    private rawListener = (e: KeyboardEvent) => {
        if ((e.ctrlKey || e.metaKey) && e.key === 'Enter') {
            this.callback();
        }
    }

}