import {ElementContainer} from "./element-container";
import {ElementContainerFactory} from "./element-container-factory";
import {EscListener} from "./esc-listener";
import {CtrlEnterListener} from "./ctrl-enter-listener";

export class ObjectOrientedBootstrapModal {

    private container: ElementContainer;
    private escListener = new EscListener(this);
    private ctrlEnterListener: CtrlEnterListener;

    public onConfirm: (() => any) | null = null;

    constructor(config: OOBMConfig) {
        this.container = new ElementContainerFactory(config).createContainer();
        if (config.onConfirm) {
            this.onConfirm = config.onConfirm;
        }
        this.container.addConfirmButtonListener(this.rawOnConfirmListener);
        this.ctrlEnterListener = new CtrlEnterListener(this.rawOnConfirmListener);
        this.container.addCloseButtonListener(() => this.close());
    }

    public open() {
        this.container.insertSelf();
        this.escListener.engage();
        this.ctrlEnterListener.engage();
    }

    public close() {
        this.container.removeSelf();
        this.escListener.disengage();
        this.ctrlEnterListener.disengage();
    }

    protected get content(): HTMLElement {
        return this.container.getContentElement();
    }

    private rawOnConfirmListener = () => {
        if (this.onConfirm) {
            this.onConfirm();
        }
    };

}

export interface OOBMConfig {
    header: string;
    content?: string | HTMLElement;
    onConfirm?: () => any;
}
