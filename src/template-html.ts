import {BOOTSTRAP_CSS} from "./bootstrap-css";

export const templateHTML = `

<style>
${BOOTSTRAP_CSS}

.animate.animate.animate {
    animation: entryAnimation 350ms ease-in-out 1;
}

@keyframes entryAnimation {
  0% {
    opacity: 0;
  }

  100% {
    opacity: 1;
  }
}
</style>

<div class="modal fade show bootstrap-root" tabindex="-1" style="display: block; background-color: RGBA(0, 0, 0, 0.5);">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
        
            <div class="modal-header">
                <h5 class="modal-title">$TITLE</h5>
                <button type="button" class="btn-close" closeButton></button>
            </div>
            
            <div class="modal-body">
                $CONTENT
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" closeButton>Close</button>
                <button type="button" class="btn btn-primary" confirmButton>Confirm</button>
            </div>
            
        </div>
    </div>
</div>

`;
