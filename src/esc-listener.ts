import {ObjectOrientedBootstrapModal} from "./object-oriented-bootstrap-modal";

/**
 * This class listens to the escape key and closes this.host if esc is pressed.
 */
export class EscListener {

    constructor(private host: ObjectOrientedBootstrapModal) {
    }

    engage() {
        document.addEventListener('keyup', this.onKeyPress);
    }

    disengage() {
        document.removeEventListener('keyup', this.onKeyPress);
    }

    onKeyPress = (e: KeyboardEvent) => {
        if (e.key === 'Escape') {
            this.host.close();
        }
    }

}