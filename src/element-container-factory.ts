import {ElementContainer} from "./element-container";
import type {OOBMConfig} from "./object-oriented-bootstrap-modal";
import {templateHTML} from "./template-html";

export class ElementContainerFactory {

    constructor(private config: OOBMConfig) {
    }

    createContainer(): ElementContainer {

        const root = document.createElement('div');
        ElementContainerFactory.setRootStyles(root);

        const shadowRoot = root.attachShadow({mode: "closed"});
        this.mountTemplate(shadowRoot);

        const container = new ElementContainer(root, shadowRoot);
        if (this.config.content instanceof HTMLElement) {
            container.getContentElement().appendChild(this.config.content);
        }
        return container;

    }

    private static setRootStyles(root: HTMLDivElement) {
        root.style.all = 'initial';
    }

    private mountTemplate(shadowRoot: ShadowRoot) {

        let html = templateHTML.replace('$TITLE', this.config.header);

        if (typeof this.config.content === 'string') {
            html = html.replace('$CONTENT', this.config.content);
        } else {
            html = html.replace('$CONTENT', '');
        }

        shadowRoot.innerHTML = html;

    }

}